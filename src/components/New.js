import React, { Component } from "react";
import axios from "axios";

class New extends Component {
  constructor(props) {
    super(props)
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this);
    this.onChangeBirthdate = this.onChangeBirthdate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
        user_name: '',
        user_email: '',
        phone_number: '',
        birthdate:''
    }
}

onChangeUserName(e) {
  this.setState({ user_name: e.target.value })
}

onChangeUserEmail(e) {
  this.setState({ user_email: e.target.value })
}

onChangePhoneNumber(e) {
  this.setState({ phone_number: e.target.value })
}

onChangeBirthdate(e) {
  this.setState({ birthdate: e.target.value })
}

onSubmit(e) {
  e.preventDefault()
  const userObject = {
      user_name: this.state.user_name,
      user_email: this.state.user_email,
      phone_number: this.state.phone_number,
      birthdate:this.state.birthdate
    };

  axios.post('https://shielded-eyrie-13804.herokuapp.com/api/users', userObject)
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });
  this.setState({ user_name: '', user_email: '', phone_number: '', birthdate: '' });
          };



  render() {
    return (
      <div>
        <div>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label>Name</label>
                        <input type="text" value={this.state.user_name} onChange={this.onChangeUserName}/>
                    </div>
                    <div>
                        <label>Email</label>
                        <input type="text" value={this.state.user_email} onChange={this.onChangeUserEmail}/>
                    </div>
                    <div>
                        <label>Phone number</label>
                        <input type="number" value={this.state.phone_number} onChange={this.onChangePhoneNumber}/>
                    </div>
                    <div>
                        <label>Birthdate</label>
                        <input type="date" value={this.state.birthdate} onChange={this.onChangeBirthdate}/>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Create User" />
                    </div>
                </form>
            </div>
      </div>
    );
  }
}
export default New;
