import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class User extends Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  loadUsers() {
    axios
      .get('https://shielded-eyrie-13804.herokuapp.com/api/users')
      .then((res) => {
        this.setState({ users: res.data });
      })
      .catch((error) => console.log(error));
  }

  deleteUsers(id, e){
    axios
    .delete(`https://shielded-eyrie-13804.herokuapp.com/api/users/${id}`)
    .then((res) => {
      const users = this.state.users.filter(item => item.id !== id);
        this.setState({ users });
    })
    .catch((error) => console.log(error));
  }

  componentDidMount() {
    this.loadUsers();
  }



  render() {

    return (
      <div>
        <h1>Users list</h1>

        <table>
          <button> <Link to='users/new'>Create user</Link> </button>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone number</th>
                <th>Birthdate</th>
                <th>Actions</th>
              </tr>
              {this.state.users.map((user) => {
                return (
                      <tr>
                        <th>{user.id}</th>
                        <th>{user.user_name}</th>
                        <th>{user.user_email}</th>
                        <th>{user.phone_number}</th>
                        <th>{user.birthdate}</th>
                        <button> <Link to={`users/edit/${user.id}`}>Edit</Link> </button>
                        <button onClick={(e) => this.deleteUsers(user.id, e)}>Delete</button>
                      </tr>
                );
              })}
        </table>
        </div>

    );
  }
}

export default User
